var request = require('request');

module.exports = function(key, host) {

	var sendEmail = function(data, callback) {
		request
			.post({
					headers: {
						'Authentication': key
					},
					url: host + "/send/email",
					json: true,
					body: { data: data }
				},
				function(error, response, body) {
					callback(error, response);
				});
	}

	var sendPush = function(data, callback) {
		request
			.post({
					headers: {
						'content-type': 'application/json',
						'Authentication': key
					},
					url: host + "/send/push",
					json: true,
					body: { data: data }
				},
				function(error, response, body) {
					callback(error, response);
				});
	}

	var sendSms = function(data, callback) {
		request
			.post({
					headers: {
						'content-type': 'application/json',
						'Authentication': key
					},
					url: host + "/send/sms",
					json: true,
					body: { data: data }
				},
				function(error, response, body) {
					callback(error, response);
				});
	}

	return {
		sendEmail: sendEmail,
		sendSms: sendSms,
		sendPush: sendPush
	}
}